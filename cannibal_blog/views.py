from django.shortcuts import render, get_object_or_404
from datetime import datetime
from cannibal_blog.models import Post


def index(request):
    post_list = Post.objects.all()
    context = {'watch_time': datetime.now(), 'post_list': post_list}
    return render(request, 'index.html', context)


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    context = {'post': post}
    return render(request, 'post_detail.html', context)
